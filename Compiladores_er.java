package compiladores_er;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Compiladores_er {

    
    public static void main(String[] args) {
        
        //EJERCICIO 1
        System.out.println("1-A) Buscar 3a en la palabra, minusculas:");
        
        System.out.println(busca_3a("mañana"));
        System.out.println(busca_3a("banana"));
        System.out.println(busca_3a("CAMARA"));
        
        System.out.println("1-B) Buscar 3a en la palabra, minusculas Y mayúsculas:");
        
        System.out.println(busca_3a_2("mañana"));
        System.out.println(busca_3a_2("banana"));
        System.out.println(busca_3a_2("CAMARA"));
    
        //EJERCICIO 2
        System.out.println("2) Validar Fecha numérica");
        
        String regexp = "\\d{1,2}/\\d{1,2}/\\d{4}";
        
        System.out.println(Pattern.matches(regexp, "11/12/2014" ));
        System.out.println(Pattern.matches(regexp, "1/12/2014" ));
        System.out.println(Pattern.matches(regexp, "11/2/2014" ));
        
        System.out.println(Pattern.matches(regexp, "11/12/14")); // El año no tiene cuatro cifras
        System.out.println(Pattern.matches(regexp, "11//2014")); // el mes no tiene una o dos cifras
        System.out.println(Pattern.matches(regexp, "11/12/14perico" )); // Sobra "perico"
        
        
        //EJERCICIO 3
        System.out.println("3)Validar fecha con mes tipo caracter:");
        String literalMonthRegexp = "\\d{1,2}/(?i)(ene|feb|mar|abr|may|jun|jul|ago|sep|oct|nov|dic)/\\d{4}";
        
        System.out.println(Pattern.matches(literalMonthRegexp, "11/dic/2014"));
        System.out.println(Pattern.matches(literalMonthRegexp, "1/nov/2014"));
        System.out.println(Pattern.matches(literalMonthRegexp, "1/AGO/2014")); 
        System.out.println(Pattern.matches(literalMonthRegexp, "21/Oct/2014")); 
        System.out.println(Pattern.matches(literalMonthRegexp, "11/abc/2014")); 
        System.out.println(Pattern.matches(literalMonthRegexp, "11//2014")); 
        System.out.println(Pattern.matches(literalMonthRegexp, "11/jul/2014perico")); 
        
        //EJERCICIO 4
        System.out.println("4) Validar número entero positivo en cadena:");
        System.out.println(validaNumeroEnteroPositivo_Exp("3"));
        System.out.println(validaNumeroEnteroPositivo_Exp("1256"));
        System.out.println(validaNumeroEnteroPositivo_Exp("-4"));
        System.out.println(validaNumeroEnteroPositivo_Exp("7"));
        
        //EJERCICIO 5
        System.out.println("5) Validar número entero negativo en cadena:");
        System.out.println(validaNumeroEnteroNegativo_Exp("-43"));
        System.out.println(validaNumeroEnteroNegativo_Exp("6"));
        System.out.println(validaNumeroEnteroNegativo_Exp("-34"));
        System.out.println(validaNumeroEnteroNegativo_Exp("-54"));
        
        //EJERCICIO 6
        System.out.println("6) Validar número entero en cadena:");
        System.out.println(validaNumeroEntero_Exp("23"));
        System.out.println(validaNumeroEntero_Exp("-23"));
        
        //EJERCICIO 7
        System.out.println("7-A) Validar una matrícula europea: ");
        System.out.println(validarMatriculaEuropea_Exp("4576ART"));
        System.out.println(validarMatriculaEuropea_Exp("Al052067"));
        
        System.out.println("7-B) Validar matrícula UAC");
        System.out.println(validarMatriculaUAC_Exp("4576ART"));
        System.out.println(validarMatriculaUAC_Exp("al052067"));
        
        //EJERCICIO 8
        System.out.println("8) Validar número binario:");
        System.out.println(validarBinario_Exp("00101101"));
        System.out.println(validarBinario_Exp("00101121"));
        
        //EJERCICIO 9
        System.out.println("9) Validar cuenta Twitter:");
        System.out.println(validarUsuarioTwitter_Exp("@carezse2344_"));
        
        //EJERCICIO 10
        System.out.println("10) Validar DNI");
        String dniRegexp = "\\d{8}[A-HJ-NP-TV-Z]" ;
        
        System.out.println(Pattern.matches(dniRegexp, "01234567C" ));
        System.out.println(Pattern.matches(dniRegexp, "01234567U" )); 
        System.out.println(Pattern.matches(dniRegexp, "0123567X")); 
        
        //EJERCICIO 11
        System.out.println("11) Validar email");
        String emailRegexp = "[^@]+@[^@]+\\.[a-zA-Z]{2,}";

        System.out.println(Pattern.matches(emailRegexp, "a@b.com"));
        System.out.println(Pattern.matches(emailRegexp, "+++@+++.com"));
        System.out.println(Pattern.matches(emailRegexp, "@b.com")); 
        System.out.println(Pattern.matches(emailRegexp, "a@b.c"));
    }
    
    public static boolean busca_3a(String cadena){
        String  expR = ".a.a.a";
        Pattern pat = Pattern.compile(expR);
        Matcher mat = pat.matcher(cadena);                                                                                   
        return mat.matches();
    }

    public static boolean busca_3a_2(String cadena){
        String  expR = ".[aA].[aA].[aA]";
        Pattern pat = Pattern.compile(expR);
        Matcher mat = pat.matcher(cadena);                                                                                   
        return mat.matches();
    }
 
    public static boolean validaNumeroEnteroPositivo_Exp(String texto){
        return texto.matches("^[0-9]+$");
    }

    public static boolean validaNumeroEnteroNegativo_Exp(String texto){
        return texto.matches("^-[0-9]+$");
    }
    
    public static boolean validaNumeroEntero_Exp(String texto){
        return texto.matches("^-?[0-9]+$");
    }
    
    public static boolean validarMatriculaEuropea_Exp(String matricula) {
        return matricula.matches("^[0-9]{4}[A-Z]{3}$");
    }
    
    public static boolean validarMatriculaUAC_Exp(String matricula) {
        return matricula.matches("^[Aa]l0[0-9]{5}$");
    }
    
    public static boolean validarBinario_Exp(String binario){
        return binario.matches("^[0-1]+$");
    }
    
    public static boolean validarUsuarioTwitter_Exp(String usuario_twitter) {
        return usuario_twitter.matches("^@([A-Za-z0-9_-]{1,15})$");
    }
}